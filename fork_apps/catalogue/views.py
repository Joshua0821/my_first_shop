from oscar.apps.catalogue.views import ProductDetailView as CoreDetailView

class MyProductView(CoreDetailView):
    template_name = 'oscar/catalogue/detail.html'