from django.db import models
from oscar.apps.catalogue.abstract_models import AbstractProduct


class Product(AbstractProduct):
    active = models.BooleanField(default=False)
    fav = models.BooleanField(verbose_name='Favorite', default=True)
    video_url = models.URLField(max_length=1000, verbose_name="Video URL",
                default="https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/1080/Big_Buck_Bunny_1080_10s_1MB.mp4")


# putting this import down so the django won't use the library's one
from oscar.apps.catalogue.models import *  # noqa isort:skip